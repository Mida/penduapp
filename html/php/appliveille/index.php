<!doctype html>
<html>
<head>
	<meta charset>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
			<!-- PROBLEME AVEC LE LOGO
		-->
			<a class="navbar-brand" href="#">
				PopSchool
        <!-- <img alt="Brand" src="img/logo_pop.jpg"> -->
      </a>
    </div>
    <div class="navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">LIEN</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Trier par<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="etudiant.php">Etudiant</a></li>
            <li><a href="#">Veilles de la semaine</a></li>
            <li><a href="#">Veilles du mois</a></li>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="register.php">S'inscrire</a></li>
        <li><a href="login.php">Se connecter</a></li>
      </ul>
    </div>
  </div>
</nav>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>

</body>
</html>

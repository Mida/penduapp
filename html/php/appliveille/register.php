<!doctype html>
<html>
<head>
	<meta charset>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

	<nav class="navbar navbar-inverse">
	  <div class="container">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="index.php">PopSchool</a>
	    </div>
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class="active"><a href="#">LIEN</a></li>
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Trier par<span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            <li><a href="#">Par étudiant</a></li>
	            <li><a href="#">Veille de la semaine</a></li>
	            <li><a href="#">Veille du mois</a></li>
	          </ul>
	        </li>
	      </ul>
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="register.php">S'inscrire</a></li>
	        <li><a href="login.php">Se connecter</a></li>
	      </ul>
	    </div>
	  </div>
	</nav>
<?php


if(isset($_POST["submit"])){
   $username=$_POST["username"];
   $password=$_POST["password"];
   $name=$_POST["name"];
   $f_name=$_POST["f_name"];
   $promo_id=$_POST["promo"];

   function promo_def($promo_id) {
  if($promo_id==1){
      return "Ada Lovelace";
      }
  if($promo_id==2){
      return "Alan Turing";
      }
   }

   $promo= promo_def($promo_id);


   $repeatpassword=$_POST["repeatpassword"];
   if($username&&$password&&$repeatpassword) {
      if ($password==$repeatpassword){
         $password=sha1($password);
         $handle=mysqli_connect("localhost","root","1234","veilleApp") or die('Error');

         $query="SELECT * FROM users WHERE username= '$username'";
         $result = mysqli_query($handle,$query);
         if($handle->affected_rows > 0) {
               echo "Ce pseudo n'est pas disponible";
               } else {
            $query="INSERT INTO `users` (nom,prenom,username,password,promo) VALUES ('$name','$f_name','$username','$password','$promo')";
            $result = mysqli_query($handle,$query);

            die("Inscription terminée <a href='login.php'>connectez vous</a>");

            }
      } else {
         echo "Les deux passwords doivent être identiques";
         }
   } else {
   echo "Veuillez saisir tous les champs";
      }
}

?>

<div class="container">
	<form method="POST" action="register.php">
		<div class="form-group">
			<label for="username">Pseudo</label>
			<input type="text" class="form-control" name="username" placeholder="Votre identifiant de connexion">
		</div>
		<div class="form-group">
			<label for="name">Nom</label>
			<input type="text" class="form-control" name="name" placeholder="Votre nom">
		</div>
	  <div class="form-group">
	    <label for="f_name">Prénom</label>
	    <input type="text" class="form-control" name="f_name" placeholder="Votre prénom">
	  </div>
		<div class="form-group">
		<label>Promotion</label>
		<select name="promo" class="form-control">
			<option value="1">Ada Lovelace</option>
			<option value="2">Alan Turing</option>
		</select>
	</div>
	  <div class="form-group">
	    <label for="password">Mot de passe</label>
	    <input type="password" class="form-control" name="password" placeholder="Mot de passe">
	  </div>
		<div class="form-group">
			<label for="repeatpassword">Confirmer le mot de passe</label>
			<input type="password" class="form-control" name="repeatpassword" placeholder="Confirmer le mot de passe">
		</div>
	  <button type="submit" name ="submit" class="btn btn-info">S'inscrire</button>
	</form>

<!-- <form method="POST" action="register.php">
<label>Votre Pseudo</label><input type="text" name="username"><br>
<label>Votre Nom</label><input type="text" name="name"><br>
<label>Votre Prénom</label><input type="text" name="f_name"><br>
<label>Votre Promo</label><select name="promo"><option value="1">Ada Lovelace</option><option value="2">Alan Turing</option></select><br>
<label>Votre Password</label><input type="password" name="password"><br>
<label>Répétez votre Password</label><input type="password" name="repeatpassword"><br>
<input type="submit" value="S'inscrire" name="submit">
</form> -->

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>


</body>
</html>

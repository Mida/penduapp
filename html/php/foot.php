<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title> Equipe de foot </title>
  <link rel="stylesheet" href="foot.css" type="text/css" media="screen">
</head>

<body>

  <?php
  #ini_set("display_errors","true");
  $handle=mysqli_connect("localhost","root","","foot");

  ?>

  <div>
    <?php
    $query="SELECT * FROM equipes";
    $result=mysqli_query($handle,$query);
    echo "<ul>";
    while($line=mysqli_fetch_array($result)) {
      echo "\t<li>";
      echo "[".$line["id"]."] ".$line["pays"]." ".$line["surnom"];
      echo "<a href=\"eq-suppr.php?id=" . $line["id"] . "\"> Supprimer </a>";
      echo "|";
      echo "<a href=\"eq-mod.php?id=" . $line["id"] . "\"> Modifier </a>";
      echo "</li>\n";
    }
    echo "</ul>";

    ?>
  </div>
<div class="case">
  <form action="eq-ajout.php" method="post">
    <label for="eqpays"><div class="formulaire">Pays </div></label>
    <input type="text" name="pays">
    <label for="eqsurnom"> <div class="formulaire"> Surnom </div> </label>
    <input type="text" name="surnom">
    <input type="submit" value="Ajouter">
  </form>
</div>
  <div>
    <?php
    $query="SELECT * FROM joueurs";
    $result=mysqli_query($handle,$query);
    echo "<ul>";
    while($line=mysqli_fetch_array($result)) {
      echo "\t<li>";
      echo "[".$line["id"]."] ".$line["prenom"]." ".$line["nom"]." ".$line["datenaiss"]." ".$line["idequipe"];
      echo "<a href=\"eq-suppr.php?id=" . $line["id"] . "\"> Supprimer </a>";
      echo "|";
      echo "<a href=\"eq-mod.php?id=" . $line["id"] . "\"> Modifier </a>";
      echo "</li>\n";
    }
    echo "</ul>";

    ?>
  </div>
<div class="case">
  <form action="eq-ajout.php" method="post">
    <label for="prenom"> <div class="formulaire">Prenom </div> </label>
    <input type="text" name="prenom">
    <label for="nom"> <div class="formulaire"> Nom </div> </label>
    <input type="text" name="nom">
    <label for="date"> <div class="formulaire">Date De Naissance </div> </label>
    <input type="text" name="date">
    <input type="submit" value="Ajouter">
  </form>
</div>
</body>

</html>

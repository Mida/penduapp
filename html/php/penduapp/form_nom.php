<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title>Formulaire Nom</title>
  <link href="style.css" rel="stylesheet">
</head>

<body>

  <form action="update_nom.php" method="GET">
    <label for="nom">Modification du nom :</label>
    <input name="nom" type="text">
    <input type="hidden" name="id" value="<?php echo $_GET["id"]; ?>" >
    <input type="submit" class="sub">
  </form>

  <div>Retour à <a href="nom.php"> la page "Nom"</div>

</body>

</html>
